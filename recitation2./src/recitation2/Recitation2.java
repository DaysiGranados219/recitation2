/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recitation2;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author daysi
 */
public class Recitation2 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        HBox pane=new HBox(20);
        Button btn = new Button();
        Button btn2=new Button();
        btn.setText("Say 'Hello World'");
        btn2.setText("Say 'GoodBye Cruel World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        btn2.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){
                System.out.print("GoodBye Cruel World");
            }
        });
        
        pane.getChildren().addAll(btn,btn2);
        
        Scene scene = new Scene(pane,400,400);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
